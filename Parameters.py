# Parameters

# MATLAB Graph Data Filepath
add20MatlabFilePath = "add20.mat"
add32MatlabFilePath = "add32.mat"
threeEltMatlabFilePath = "3elt.mat"
fourEltMatlabFilePath = "barth5.mat"

matlabFilePath = "Random" # To generate a Random Graph, set this field to any random string

# Random Graph Parameters
randomGraphNodeSize = 100
probabilityEdgeCreation = 0.5

# Random Weights Parameters
minRandomWeight = 1
maxRandomWeight = 20

# Binary Hybrid Grey Wolf Optimizer Parameters
maxNumberOfCutsTries = 200
safetyFactorForMaxNumberOfCuts = 1.2

W1 = 0.5  # Min-Cut Objective Weight
W2 = 0.5 # Balance Objective Weight

numberOfSearchAgents = 10
maxNumberOfIterations = 1000