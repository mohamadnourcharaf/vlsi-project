class Result:

    def __init__(self, iterationNumber, alphaScore, alphaPositions, f1, f2, numberOfCuts, areaDifference):
        self.iterationNumber = iterationNumber
        self.alphaScore = alphaScore
        self.alphaPositions = alphaPositions
        self.f1 = f1
        self.f2 = f2
        self.numberOfCuts = numberOfCuts
        self.areaDifference = areaDifference
