import numpy as np

import Parameters
import DataManager
import BinaryHybridGreyWolfOptimizer


def start():

    # Binary Hybrid Grey Wolf Optimization on MATLAB Graph Data
    binaryHybridGreyWolfOptimizerOnMATLABGraphData(Parameters.matlabFilePath)


def binaryHybridGreyWolfOptimizerOnMATLABGraphData(matlabFilePath):

    # Get Nodes from Matlab Graph Data
    nodes = DataManager.getGraphFromMatlabFile(matlabFilePath)

    # Set Node Weights Randomly
    DataManager.setNodeWeightsRandomly(nodes,Parameters.minRandomWeight,Parameters.maxRandomWeight)

    # Partition with Binary Hybrid Grey Wolf Optimization

    # Get Max possible number of cuts using approximation
    maxNumberOfCuts = approximateMaxNumberOfCuts(nodes,Parameters.maxNumberOfCutsTries) * Parameters.safetyFactorForMaxNumberOfCuts

    # Get Max possible area difference using approximation
    totalArea = 0
    for node in nodes:
        totalArea += node.weight
    maxAreaDifference = totalArea

    results = BinaryHybridGreyWolfOptimizer.binaryHybridGreyWolfOptimizer(Parameters.numberOfSearchAgents,Parameters.maxNumberOfIterations,len(nodes), nodes, maxNumberOfCuts, maxAreaDifference, Parameters.W1, Parameters.W2)


def approximateMaxNumberOfCuts(nodes,numberOfTries):

    maxNumberOfCuts = 0
    for i in range(numberOfTries):

        positions = (np.random.uniform(size=[1, len(nodes)]) > 0.5) * 1.0

        BinaryHybridGreyWolfOptimizer.updateNodePartitionsWithPositions(nodes,positions[0])

        numberOfCuts = BinaryHybridGreyWolfOptimizer.calculateNumberOfCuts(nodes)

        if numberOfCuts > maxNumberOfCuts:
            maxNumberOfCuts = numberOfCuts

    return maxNumberOfCuts


start()
