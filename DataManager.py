import scipy.io
import networkx as nx

import random

import Parameters
import Node
import Edge


def getGraphFromMatlabFile(matlabFilePath):

    if matlabFilePath == Parameters.add20MatlabFilePath:
        A = scipy.io.loadmat(matlabFilePath)["Problem"][0][0][1].toarray()
    elif matlabFilePath == Parameters.add32MatlabFilePath:
        A = scipy.io.loadmat(matlabFilePath)["Problem"][0][0][1].toarray()
    elif matlabFilePath == Parameters.threeEltMatlabFilePath:
        A = scipy.io.loadmat(matlabFilePath)["Problem"][0][0][2].toarray()
    elif matlabFilePath == Parameters.fourEltMatlabFilePath:
        A = scipy.io.loadmat(matlabFilePath)["Problem"][0][0][0].toarray()
    else:
        A = None
        print("Invalid file - Will generate a Random Graph instead")

    if A is not None:
        G = nx.from_numpy_matrix(A)
    else:
        G = nx.fast_gnp_random_graph(n=Parameters.randomGraphNodeSize, p=Parameters.probabilityEdgeCreation, seed=None, directed=False)

    nodes = processGraphToNodes(G)

    return nodes


def processGraphToNodes(G):

    gNodes = list(G.nodes())
    gEdges = list(G.edges())

    nodes = []

    for i in range(0,len(gNodes)):
        node = Node.Node()
        node.id = i
        nodes.append(node)

    for i in range(0,len(gEdges)):
        gEdge = gEdges[i]
        edge = Edge.Edge()
        for nodeIndex in gEdge:
            node = nodes[nodeIndex]
            edge.nodes.append(node)
            node.edges.append(edge)

    return nodes


def setNodeWeightsRandomly(nodes,minRandomWeight,maxRandomWeight):
    for node in nodes:
        node.weight = random.randrange(maxRandomWeight) + minRandomWeight