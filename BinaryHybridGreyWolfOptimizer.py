import numpy as np
import matplotlib.pyplot as plt

import Result


np.random.seed(42)


# Binary Hybrid Grey Wolf Optimizer
def binaryHybridGreyWolfOptimizer(numberOfSearchAgents, maxNumberOfIterations, numberOfDimensions, nodes, maxNumberOfCuts, maxAreaDifference, W1, W2):

    # Prepare Convergence Curve
    results = []

    # Note: change scores to - inf for maximization problems
    # Initialize Alpha Positions
    alphaPositions = np.zeros(numberOfDimensions)
    alphaScore = np.inf

    # Initialize Beta Positions
    betaPositions = np.zeros(numberOfDimensions)
    betaScore = np.inf

    # Initialize Delta Positions
    deltaPositions = np.zeros(numberOfDimensions)
    deltaScore = np.inf

    # Initialize the positions of the first population of search agents
    positions = (np.random.uniform(size=[numberOfSearchAgents, numberOfDimensions]) > 0.5) * 1.0

    # Initialize Velocity
    velocity = 0.3 * np.random.normal(size=[numberOfSearchAgents, numberOfDimensions])

    # Initialize w
    w = 0.5 + np.random.uniform() / 2

    # Loop counter
    l = 0

    # Main loop
    while l < maxNumberOfIterations:

        for i in range(numberOfSearchAgents):

            # Calculate objective function for each search agent
            fitness = calculateFitness(positions[i, :], nodes, maxNumberOfCuts, maxAreaDifference, W1, W2)

            # Update Alpha
            if fitness < alphaScore:
                alphaScore = fitness
                alphaPositions = np.copy(positions[i, :])

            # Update Beta
            if fitness > alphaScore and fitness < betaScore:
                betaScore = fitness
                betaPositions = np.copy(positions[i, :])

            # Update Delta
            if fitness > alphaScore and fitness > betaScore and fitness < deltaScore:
                deltaScore = fitness
                deltaPositions = np.copy(positions[i, :])

        # Update value of a, Note: a decreases linearly from 2 to 0
        a = 2 - l * (2 / (maxNumberOfIterations - 1)) # (8)

        # Update the Position of Search Agents, including the Omegas
        for i in range(numberOfSearchAgents):

            for j in range(numberOfDimensions):

                r1 = np.random.uniform()
                A1 = 2 * a * r1 - a  # Equation(3.3)
                C1 = 0.5
                D_alpha = np.abs(C1 * alphaPositions[j] - w * positions[i, j])  # Equation(3.5) - part 1 #(19)
                v1 = sigmoid(-1 * A1 * D_alpha)  # (18)
                v1 = 0 if v1 < np.random.uniform() else 1
                X1 = ((alphaPositions[j] + v1) >= 1) * 1.0

                r1 = np.random.uniform()
                A2 = 2 * a * r1 - a  # Equation(3.3)
                C2 = 0.5
                D_beta = np.abs(C2 * betaPositions[j] - w * positions[i, j])  # Equation(3.5) - part 2 #(19)
                v1 = sigmoid(-1 * A2 * D_beta)  # (18)
                v1 = 0 if v1 < np.random.uniform() else 1
                X2 = ((betaPositions[j] + v1) >= 1) * 1.0

                r1 = np.random.uniform()
                A3 = 2 * a * r1 - a  # Equation(3.3)
                C3 = 0.5
                D_delta = np.abs(C3 * deltaPositions[j] - w * positions[i, j])  # Equation(3.5) - part 3 #(19)
                v1 = sigmoid(-1 * A3 * D_delta)  # (18)
                v1 = 0 if v1 < np.random.uniform() else 1
                X3 = ((deltaPositions[j] + v1) >= 1) * 1.0

                # Update Velocity # (20)
                r1 = np.random.uniform()
                r2 = np.random.uniform()
                r3 = np.random.uniform()
                velocity[i, j] = w * (velocity[i, j]
                                      + C1 * r1 * (X1 - positions[i, j])
                                      + C2 * r2 * (X2 - positions[i, j])
                                      + C3 * r3 * (X3 - positions[i, j]))

                # Update Positions # (21)
                xx = sigmoid((X1 + X2 + X3) / 3) + velocity[i, j]
                xx = 0 if xx < np.random.uniform() else 1
                positions[i, j] = xx  # Equation (3.7)

        # Update Results
        result = getResult(l, alphaScore, alphaPositions, nodes, maxNumberOfCuts, maxAreaDifference)
        results.append(result)

        # Log Result
        logResult(result, maxNumberOfCuts, maxAreaDifference)

        # Increment l
        l = l + 1

    # Generate Plots from Results
    generatePlots(results)

    return results


def sigmoid(x):
    return 1 / (1 + np.exp(-10 * (x - 0.5))) # (15)


def calculateFitness(positions, nodes, maxNumberOfCuts, maxAreaDifference, W1, W2):

    # Update Node Partitions with Positions
    updateNodePartitionsWithPositions(nodes,positions)

    # Calculate Number of cuts
    numberOfCuts = calculateNumberOfCuts(nodes)

    # Calculate Areas A and B, and their difference
    areaDifference = calculateAreaDifference(nodes)

    # Minimize F1: Min-Cut Objective Function
    f1 = calculateF1(numberOfCuts,maxNumberOfCuts)

    # Minimize F2: Balance Objective Function
    f2 = calculateF2(areaDifference,maxAreaDifference)

    if f1 > 1 or f2 > 1:
        print(
            "Error: These functions must strictly be between 0 and 1, please try again or recheck implementation")
        exit(0)

    # Minimize Fitness: This is the objective function to minimize
    fitness = + (W1 * f1) + (W2 * f2)

    return fitness


def calculateF2(areaDifference,maxAreaDifference):
    f2 = 1 if maxAreaDifference == 0 else areaDifference / maxAreaDifference
    return f2


def calculateF1(numberOfCuts,maxNumberOfCuts):
    f1 = 1 if maxNumberOfCuts == 0 else numberOfCuts / maxNumberOfCuts
    return f1


def calculateAreaDifference(nodes):
    areaA = 0
    areaB = 0
    for i in range(0, len(nodes)):
        if nodes[i].partition == 0:
            areaA += nodes[i].weight
        else:
            areaB += nodes[i].weight
    areaDifference = abs(areaA - areaB)

    return areaDifference


def calculateNumberOfCuts(nodes):

    numberOfCuts = 0
    for i in range(0, len(nodes)):
        node = nodes[i]
        if node.partition == 0:
            for edge in node.edges:
                for edgeNode in edge.nodes:
                    if edgeNode.partition == node.partition:
                        continue
                    else:
                        numberOfCuts += 1

    return numberOfCuts


def updateNodePartitionsWithPositions(nodes,positions):
    for i in range(0, len(positions)):
        nodes[i].partition = positions[i]


def getResult(iterationNumber,alphaScore,alphaPositions,nodes,maxNumberOfCuts,maxAreaDifference):

    updateNodePartitionsWithPositions(nodes,alphaPositions)
    numberOfCuts = calculateNumberOfCuts(nodes)
    areaDifference = calculateAreaDifference(nodes)
    f1 = calculateF1(numberOfCuts,maxNumberOfCuts)
    f2 = calculateF2(areaDifference,maxAreaDifference)

    result = Result.Result(iterationNumber, alphaScore, alphaPositions, f1, f2, numberOfCuts, areaDifference)

    return result


def logResult(result, maxNumberOfCuts, maxAreaDifference):
    print("Iteration Number: " + str(result.iterationNumber + 1))
    print("Number of Cuts: " + str(result.numberOfCuts))
    print("Area Difference: " + str(result.areaDifference) + " Max Area Difference: " + str(
        maxAreaDifference))
    print("F1: " + str(result.f1) + " F2: " + str(result.f2))
    print("Alpha Score: " + str(result.alphaScore))
    print("Positions:")
    print(result.alphaPositions)
    print("")
    print("****************************************************************************************************")


def generatePlots(results):
    iterations = []
    numberOfCutsVariation = []
    areaDifferenceVariation = []
    alphaScoreVariation = []
    for i in range(0, len(results)):
        result = results[i]
        iterations.append(i)
        numberOfCutsVariation.append(result.numberOfCuts)
        areaDifferenceVariation.append(result.areaDifference)
        alphaScoreVariation.append(result.alphaScore)

    xLabel = "Iteration Number"
    yArray = [numberOfCutsVariation,areaDifferenceVariation,alphaScoreVariation]
    yLabelArray = ["Number of Cuts", "Area A-B Difference", "Alpha Score"]

    for i in range(0,len(yArray)):
        y = yArray[i]
        yLabel = yLabelArray[i]
        title = yLabel + " vs " + xLabel
        plt.title(title)
        plt.plot(iterations, y)
        plt.xlabel(xLabel)
        plt.ylabel(yLabel)
        plt.grid()
        plt.savefig("plots/" + str(yLabel) + ".png")
        plt.close()